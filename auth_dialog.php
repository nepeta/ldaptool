<?php

declare(strict_types = 1);

/**
 * Interactively obtain credentials for a given domain.
 *
 * @param string $domain
 * @param string $default_username
 * @return array|null An array in the form [$username, $password] or null on input failure.
 */
function auth_dialog(string $domain = null, string $default_username = null): ?array {
    if ($domain !== null) {
        echo "Authenticating against $domain\n";
    }

    do {
        if ($default_username !== null) {
            echo "Username ($default_username): ";
        } else {
            echo "Username: ";
        }
        $username = fgets(STDIN);
        if ($username === false) {
            return null;
        }
        $username = rtrim($username, "\r\n");
    } while (!strlen($username) && !($username = $default_username));

    system("stty -echo 2>/dev/null");
    echo "Password: ";
    $password = fgets(STDIN);
    echo "\n";
    if ($password === false) {
        return null;
    }
    $password = rtrim($password, "\r\n");
    system("stty echo 2>/dev/null");

    return [$username, $password];
}

