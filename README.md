# ldaptool

A hacked-together toy for exploring LDAP directories.

## Requirements

* PHP 7.1 with the LDAP module enabled
* OpenLDAP

You may need to adjust OpenLDAP to be able to verify CampusAD's TLS certificate.
On Enterprise Linux 7 and Fedora 27, OpenLDAP doesn't ship configured to load
any certificate authority certificates. To configure it to use the certificates
already installed on most systems, add this line to `/etc/openldap/ldap.conf`:

    TLS_CACERT /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem

## Configuration

Many of the configuration options can be sourced from configuration files. I
include working configuration files for Campus AD, RHSNET, and People Search.

## Examples
1. Get an overview of attribute distribution in RHSNET:

   ```sh
   ldaptool -i rhsnet.ini -s
   ```

2. Search for yourself in MSU's public directory:

   ```sh
   ldaptool -i msudir.ini -f uid=$(whoami)
   ```

3. Find a NetID via the public directory:

   ```sh
   ldaptool -i msudir.ini -a uid -f "cn=Renee Margaret McConahy"
   ```

4. List all employees for a supervisor:

   ```sh
    ldaptool -i campusad.ini \
       -f "manager=CN=farmerl,OU=Accounts,DC=campusad,DC=msu,DC=edu" \
       -a displayname -a title
   ```

## Troubleshooting
For tracing LDAP problems, consider turning on LDAP's debugging:

```php
ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);
```
